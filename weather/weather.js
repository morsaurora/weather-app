const request = require('request');

var getWeather = (lat,lng) => {
    request({
        url: `https://api.darksky.net/forecast/a27259c2e809b561395e8d55f86224ef/${lat},${lng}`,
        json: true
    }, (error, response, body) => {
        if (error) {
            console.log('Unable to connect to forecast servers.');
        }
        else if(response.statusCode === 400) {
            console.log('The given location or time is invalid');
        }
        else if(response.statusCode === 403) {
            console.log('Unauthorized');
        }
        else if(response.statusCode === 200) {
            console.log(`Current temperature: ${body.currently.temperature}`);
        }
    })
}

module.exports = {
    getWeather
}

