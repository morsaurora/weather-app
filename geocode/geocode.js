const request = require('request');

const weather = require('../weather/weather');

var geocodeAddress = (address) => {
    const encodedAddress = encodeURIComponent(address);

    request({
        url: `http://www.mapquestapi.com/geocoding/v1/address?key=IIcXa64wTeDK8DPEhwBXjf5GvYQYNwCA&location=${encodedAddress}`,
        json: true
    }, (error, response, body) => {
        if(error) {
            console.log('Unable to connect to the geolocation servers!!')
        }
        else if(body.info.statuscode!==0) {
            console.log('Insufficient/Wrong input provided')
        }
        else if(body.info.statuscode === 0) {
            const lat = body.results[0].locations[0].latLng.lat;
            const lng = body.results[0].locations[0].latLng.lng;
            console.log(`Latitute: ${lat}`);
            console.log(`Longitude: ${lng}`);
            weather.getWeather(lat,lng);
        }
    })
}

module.exports = {
    geocodeAddress
}

